package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

const (
	MENU_COMMAND = "dmenu -b -f"
	VERSION      = "0.0.1"
)

type (
	switches struct {
		move    bool
		version bool
	}
	workspaceInfo struct {
		// id uint
		Name string `json:"name"`
		// num int
		// output string
		// focused bool
		// urgent bool
		// visible bool
		// rect geometry
	}
	// geometry struct {
	// 	X      uint
	// 	Y      uint
	// 	Width  uint
	// 	Height uint
	// }
)

func init() {
	log.SetFlags(log.Lshortfile)
	log.SetPrefix("i3wm: ")
}

func main() {
	var opts switches

	fset := flag.NewFlagSet("i3wm workspace switcher", flag.ContinueOnError)
	opts.buildFlags(fset)

	err := fset.Parse(os.Args[1:])
	if err == flag.ErrHelp {
		return
	}
	if err != nil {
		log.Fatal("Failure at flags parsing!")
	}

	switch {
	case opts.version:
		fmt.Println(VERSION)
	default:
		wsNames := getActiveWorkspaces()
		wsSelected := showMenu(wsNames, fset.Args())
		switchWorkspace(wsSelected, opts.move)
	}
}

func (r *switches) buildFlags(fs *flag.FlagSet) {
	fs.BoolVar(&r.move, "m", false, "Make currently focused client to be moved to the target workspace")
	fs.BoolVar(&r.version, "V", false, "Prints version information")
}

// Returns a list of currently active workspaces
func getActiveWorkspaces() (res []string) {
	var workspaces []workspaceInfo

	out, err := exec.Command("i3-msg", "-t", "get_workspaces").Output()
	if err != nil {
		log.Fatal(err)
	}
	if err := json.Unmarshal(out, &workspaces); err != nil {
		log.Fatal(err)
	}

	for _, ws := range workspaces {
		res = append(res, ws.Name)
	}
	return
}

// Displays menu with given options, returns a selected item
func showMenu(ws []string, args []string) string {
	menuCmd := strings.Fields(MENU_COMMAND)
	cmd := exec.Command(menuCmd[0], append(menuCmd[1:], args...)...)
	cmdIn, err := cmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		defer cmdIn.Close()
		_, err := cmdIn.Write([]byte(strings.Join(ws, "\n")))
		if err != nil {
			log.Fatal(err)
		}
	}()

	out, err := cmd.Output()
	if err != nil && len(out) > 0 {
		log.Fatal(err)
	}
	return string(out)
}

// Switches i3 to a given workspace
func switchWorkspace(ws string, move bool) {
	if len(ws) == 0 {
		return
	}
	var err error

	if !move {
		err = exec.Command("i3-msg", "workspace", ws).Start()
	} else {
		err = exec.Command("i3-msg", "move to workspace", ws).Start()
	}
	if err != nil {
		log.Fatal(err)
	}
}
